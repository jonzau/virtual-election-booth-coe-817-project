package coe817;

/**
 *
 * @author Ionathan Zauritz, Jungro Yun
 */
public class Vote {
    private int candidateId;
    private String UUID;    //this is the unique voting number

    public Vote(int candidateId, String UUID) {
        this.candidateId = candidateId;
        this.UUID = UUID;
    }

    public int getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(int candidateId) {
        this.candidateId = candidateId;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }
    
}
