package coe817;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 *
 * @author Ionathan Zauritz
 */
public class DES {

    private SecretKey key;

    public DES() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("DES");
        key = keyGen.generateKey();
    }

    public SecretKey getKey() {
        return key;
    }

    public void setKey(SecretKey key) {
        this.key = key;
    }
    
    public static String arrToString(byte[] arr) {
        String s = "";
        for (int i = 0; i < arr.length; i++) {
            s = s + (char) arr[i];
        }
        return s;
    }

    public byte[] encrypt(byte[] msg) {
        return encrypt(msg, key);
    }
    
    public byte[] encrypt(byte[] msg, SecretKey key) {
        byte[] encMsg = null;
        try {
            Cipher rsaCipher = Cipher.getInstance("DES");
            rsaCipher.init(Cipher.ENCRYPT_MODE, key);
            encMsg = rsaCipher.doFinal(msg);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(RSA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return encMsg;
    }
    
    public byte[] decrypt(byte[] msg) {
        return decrypt(msg, key);
    }
    
    public byte[] decrypt(byte[] msg, SecretKey key) {
        byte[] decMsg = null;
        try {
            Cipher rsaCipher = Cipher.getInstance("DES");
            rsaCipher.init(Cipher.DECRYPT_MODE, key);
            decMsg = rsaCipher.doFinal(msg);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(RSA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return decMsg;
    }

}
