package coe817;

import java.security.PublicKey;
import java.util.Arrays;

/**
 *
 * @author Ionathan Zauritz
 */
public class Shared {
    public static PublicKey publicKeyA;
    public static PublicKey publicKeyB;
    
    final public static int publicKeySize = 162;
    
    public static byte[] addArrays(byte[] arr1, byte[] arr2){
        byte[] arr = new byte[arr1.length+arr2.length];
        for (int i = 0; i < arr1.length; i++) {
            arr[i] = arr1[i];
        }
        for (int i = 0; i < arr2.length; i++) {
            arr[arr1.length+i] = arr2[i];
        }
        return arr;
    }
    
    public static String byteArrToString(byte[] arr) {
        String s = "";
        for (int i = 0; i < arr.length; i++) {
            s = s + (char) arr[i];
        }
        return s;
    }
    
    public static byte[] shrinkArr(byte[] b, int size){
        byte[] b2 = new byte[size];
        for (int i = 0; i < size; i++) {
            b2[i] = b[i];
        }
        return b2;
    }
    
    public static void main(String[] args) {
        byte[] a1 = {23, 55, 43};
        byte[] a2 = {10, 78, 3};
        System.out.println(Arrays.toString(a1));
        System.out.println(Arrays.toString(a2));
        System.out.println(Arrays.toString(addArrays(a1, a2)));
    }
}
