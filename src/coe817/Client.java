package coe817;

import com.sun.prism.impl.BufferUtil;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Ionathan Zauritz, Jungro Yun
 */
public class Client {

    public static void main(String[] args) throws Exception {
        System.out.println("Client:\n");
        String ID = "client";
        PublicKey CLAPublicKey = KeyFile.LoadPubKey("CLA_public.key", "RSA");
        PublicKey CTFPublicKey = KeyFile.LoadPubKey("CTF_public.key", "RSA");

        String ipCLA, ipCTF;
        int portCLA = 1001;
        int portCTF = 1002;
        if (args.length < 2) {
            ipCLA = "127.0.0.1";
            ipCTF = ipCLA;
        } else {
            ipCLA = args[0];
            ipCTF = args[1];
        }

        //connect to CLA
        Socket socketCLA = new Socket(ipCLA, portCLA);

        System.out.println("Connecting to CLA at: " + socketCLA.getInetAddress() + ":" + socketCLA.getPort() + "\n");

        DataInputStream netInCLA = new DataInputStream(socketCLA.getInputStream());
        DataOutputStream netOutCLA = new DataOutputStream(socketCLA.getOutputStream());

        //create ciphers and publish public key
        RSA rsaCipher = new RSA();
        DES desCipherCLA = new DES();
        DES desCipherCTF = new DES();

        //send public key
        byte[] msgOutCLA;
        byte[] msgOutECLA;
        msgOutCLA = rsaCipher.getKeyPair().getPublic().getEncoded();
        //split so it encrypts. needs to be split again to decrypt on the other side
        byte[] pk1 = new byte[msgOutCLA.length / 2];
        byte[] pk2 = new byte[msgOutCLA.length / 2];
        for (int i = 0; i < pk1.length; i++) {
            pk1[i] = msgOutCLA[i];
            pk2[i] = msgOutCLA[pk1.length + i];
        }
        byte[] pk1E = rsaCipher.encrypt(pk1, CLAPublicKey);
        byte[] pk2E = rsaCipher.encrypt(pk2, CLAPublicKey);
        msgOutECLA = Shared.addArrays(pk1E, pk2E);
        netOutCLA.write(msgOutECLA);

        //send N1 and ID
        Random r = new Random(new Date().getTime());
        byte[] N1 = ByteBuffer.allocate(BufferUtil.SIZEOF_INT).putInt(r.nextInt(10000)).array();

        msgOutCLA = Shared.addArrays(N1, ID.getBytes());
        msgOutECLA = rsaCipher.encrypt(msgOutCLA, CLAPublicKey);
        netOutCLA.write(msgOutECLA);

        //receive N1Received and N2       
        byte[] msgInECLA;
        int msgSize;
        msgInECLA = new byte[1024 * 1024];
        msgSize = netInCLA.read(msgInECLA);
        msgInECLA = Shared.shrinkArr(msgInECLA, msgSize);
        byte[] msgInCLA = rsaCipher.decrypt(msgInECLA, rsaCipher.getKeyPair().getPrivate());

        //process data
        byte[] N2 = new byte[BufferUtil.SIZEOF_INT];
        byte[] N1Received = new byte[BufferUtil.SIZEOF_INT];
        for (int i = 0; i < BufferUtil.SIZEOF_INT; i++) {
            N1Received[i] = msgInCLA[i];
        }
        if (!Arrays.equals(N1, N1Received)) {
            System.out.println("N1 doesn't match!");
            socketCLA.close();
            return;
        }
        for (int i = BufferUtil.SIZEOF_INT; i < msgInCLA.length; i++) {
            N2[i - BufferUtil.SIZEOF_INT] = msgInCLA[i];
        }

        //send N2
        msgOutCLA = N2;
        msgOutECLA = rsaCipher.encrypt(msgOutCLA, CLAPublicKey);
        netOutCLA.write(msgOutECLA);

        //send Ks
        byte[] KsCLA = desCipherCLA.getKey().getEncoded();
        msgOutCLA = rsaCipher.encrypt(KsCLA, rsaCipher.getKeyPair().getPrivate());
        //split so it encrypts. needs to be split again to decrypt on the other side
        byte[] m1 = new byte[msgOutCLA.length / 2];
        byte[] m2 = new byte[msgOutCLA.length / 2];
        for (int i = 0; i < m1.length; i++) {
            m1[i] = msgOutCLA[i];
            m2[i] = msgOutCLA[m1.length + i];
        }
        byte[] m1E = rsaCipher.encrypt(m1, CLAPublicKey);
        byte[] m2E = rsaCipher.encrypt(m2, CLAPublicKey);
        msgOutECLA = Shared.addArrays(m1E, m2E);
        netOutCLA.write(msgOutECLA);

        System.out.println("Secure connection established with CLA!\n");

        //connect to CTF
        Socket socketCTF = new Socket(ipCTF, portCTF);

        System.out.println("Connecting to CTF at: " + socketCTF.getInetAddress() + ":" + socketCTF.getPort() + "\n");

        DataInputStream netInCTF = new DataInputStream(socketCTF.getInputStream());
        DataOutputStream netOutCTF = new DataOutputStream(socketCTF.getOutputStream());

        //send public key
        byte[] msgOutCTF;
        byte[] msgOutECTF;
        msgOutCTF = rsaCipher.getKeyPair().getPublic().getEncoded();
        //split so it encrypts. needs to be split again to decrypt on the other side
        pk1 = new byte[msgOutCTF.length / 2];
        pk2 = new byte[msgOutCTF.length / 2];
        for (int i = 0; i < pk1.length; i++) {
            pk1[i] = msgOutCTF[i];
            pk2[i] = msgOutCTF[pk1.length + i];
        }
        pk1E = rsaCipher.encrypt(pk1, CTFPublicKey);
        pk2E = rsaCipher.encrypt(pk2, CTFPublicKey);
        msgOutECTF = Shared.addArrays(pk1E, pk2E);
        netOutCTF.write(msgOutECTF);

        //send N1 and ID
        r = new Random(new Date().getTime());
        N1 = ByteBuffer.allocate(BufferUtil.SIZEOF_INT).putInt(r.nextInt(10000)).array();

        msgOutCTF = Shared.addArrays(N1, ID.getBytes());
        msgOutECTF = rsaCipher.encrypt(msgOutCTF, CTFPublicKey);
        netOutCTF.write(msgOutECTF);

        //receive N1Received and N2       
        byte[] msgInECTF;
        msgInECTF = new byte[1024 * 1024];
        msgSize = netInCTF.read(msgInECTF);
        msgInECTF = Shared.shrinkArr(msgInECTF, msgSize);
        byte[] msgIn = rsaCipher.decrypt(msgInECTF, rsaCipher.getKeyPair().getPrivate());

        //process data
        N2 = new byte[BufferUtil.SIZEOF_INT];
        N1Received = new byte[BufferUtil.SIZEOF_INT];
        for (int i = 0; i < BufferUtil.SIZEOF_INT; i++) {
            N1Received[i] = msgIn[i];
        }
        if (!Arrays.equals(N1, N1Received)) {
            System.out.println("N1 doesn't match!");
            socketCLA.close();
            return;
        }
        for (int i = BufferUtil.SIZEOF_INT; i < msgIn.length; i++) {
            N2[i - BufferUtil.SIZEOF_INT] = msgIn[i];
        }

        //send N2
        msgOutCTF = N2;
        msgOutECTF = rsaCipher.encrypt(msgOutCTF, CTFPublicKey);
        netOutCTF.write(msgOutECTF);

        //send Ks
        byte[] KsCTF = desCipherCTF.getKey().getEncoded();
        msgOutCTF = rsaCipher.encrypt(KsCTF, rsaCipher.getKeyPair().getPrivate());
        //split so it encrypts. needs to be split again to decrypt on the other side
        m1 = new byte[msgOutCTF.length / 2];
        m2 = new byte[msgOutCTF.length / 2];
        for (int i = 0; i < m1.length; i++) {
            m1[i] = msgOutCTF[i];
            m2[i] = msgOutCTF[m1.length + i];
        }
        m1E = rsaCipher.encrypt(m1, CTFPublicKey);
        m2E = rsaCipher.encrypt(m2, CTFPublicKey);
        msgOutECTF = Shared.addArrays(m1E, m2E);
        netOutCTF.write(msgOutECTF);

        System.out.println("Secure connection established with CTF!\n");

        Scanner userIn = new Scanner(System.in);
        String SIN, password;
        while (true) {
            System.out.print("SIN: ");
            SIN = userIn.nextLine();
            System.out.print("Password: ");
            password = userIn.nextLine();

            //send credentials to CLA
            msgOutCLA = SIN.getBytes();
            msgOutECLA = desCipherCLA.encrypt(msgOutCLA, desCipherCLA.getKey());
            netOutCLA.write(msgOutECLA);

            msgOutCLA = password.getBytes();
            msgOutECLA = desCipherCLA.encrypt(msgOutCLA, desCipherCLA.getKey());
            netOutCLA.write(msgOutECLA);

            //receive UUID, or invalid msg from CLA
            msgInECLA = new byte[1024 * 1024];
            msgSize = netInCLA.read(msgInECLA);
            msgInECLA = Shared.shrinkArr(msgInECLA, msgSize);
            msgInCLA = desCipherCLA.decrypt(msgInECLA, desCipherCLA.getKey());

            String UUID;
            if (new String(msgInCLA).equals("invalid")) {
                System.out.println("You are not authorized to vote!\n"
                        + "Make sure your credentials are correct.\n");
                continue;
            } else {
                UUID = new String(msgInCLA);
            }

            //send CTF vote request
            msgOutCTF = "request".getBytes();
            //split so it encrypts. needs to be split again to decrypt on the other side
            msgOutECTF = desCipherCTF.encrypt(msgOutCTF, desCipherCTF.getKey());
            netOutCTF.write(msgOutECTF);

            //get list of candidates from CTF
            msgInECTF = new byte[1024 * 1024];
            msgSize = netInCTF.read(msgInECTF);
            msgInECTF = Shared.shrinkArr(msgInECTF, msgSize);
            msgIn = desCipherCTF.decrypt(msgInECTF, desCipherCTF.getKey());

            //display list of candidates
            String candidateList = new String(msgIn);
            System.out.println(candidateList);

            //take user candidate input
            String voteInput = "";
            boolean invalidVote = false;
            do {
                System.out.print("Enter the number of the candidate you wish to vote for.\n"
                        + "Your vote: ");

                voteInput = userIn.nextLine();

                //check if vote is valid
                if (Integer.parseInt(voteInput) < 1
                        || Integer.parseInt(voteInput) > candidateList.split("\r\n|\r|\n").length) {
                    invalidVote = true;
                    System.out.println("Invalid vote!");
                } else {
                    invalidVote = false;
                }
            } while (invalidVote);

            //send candidate number and UUID to CTF
            msgOutCTF = UUID.getBytes();
            msgOutECTF = desCipherCTF.encrypt(msgOutCTF, desCipherCTF.getKey());
            netOutCTF.write(msgOutECTF);

            msgOutCTF = voteInput.getBytes();
            msgOutECTF = desCipherCTF.encrypt(msgOutCTF, desCipherCTF.getKey());
            netOutCTF.write(msgOutECTF);

            //receive confirmation and optionally results from CTF
            msgInECTF = new byte[1024 * 1024];
            msgSize = netInCTF.read(msgInECTF);
            msgInECTF = Shared.shrinkArr(msgInECTF, msgSize);
            msgIn = desCipherCTF.decrypt(msgInECTF, desCipherCTF.getKey());

            if (new String(msgIn).equals("invalid")) {
                System.out.println("Your vote was NOT valid!\n");
            } else {
                System.out.println("\n" + Shared.byteArrToString(msgIn));
            }
        }

    }
}
