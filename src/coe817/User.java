package coe817;

/**
 *
 * @author Ionathan Zauritz, Jungro Yun
 */
public class User {
    private String SIN;
    private String password;

    public User(String SIN, String password) {
        this.SIN = SIN;
        this.password = password;
    }

    public String getSIN() {
        return SIN;
    }

    public void setSIN(String SIN) {
        this.SIN = SIN;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
