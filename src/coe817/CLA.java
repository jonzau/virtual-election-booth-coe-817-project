package coe817;

import com.sun.prism.impl.BufferUtil;
import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.spec.SecretKeySpec;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

/**
 *
 * @author Ionathan Zauritz, Jungro Yun
 */
public class CLA {

    private static Socket socketCTF;
    private static final String ID = "CLA";
    private static KeyPair keyPair;
    private static PublicKey CTFkey;

    public static void sendUUIDToCTF(String UUID) {
        DataOutputStream netOut = null;
        try {
            //send UUID to CTF
            netOut = new DataOutputStream(socketCTF.getOutputStream());
            byte[] msgOutE;
            RSA rsaCipher = new RSA();
            msgOutE = rsaCipher.encrypt(UUID.getBytes(), CTFkey);
            netOut.write(msgOutE);
        } catch (IOException | NoSuchAlgorithmException ex) {
            Logger.getLogger(CLA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void serverInstance(Socket socket) {
        String foreignID = "";
        DataOutputStream netOut = null;
        PublicKey clientPublicKey;
        try {
            //create ciphers
            RSA rsaCipher = new RSA();
            rsaCipher.setKeyPair(keyPair);
            DES desCipher = new DES();
            System.out.println("Connecting client at: " + socket.getInetAddress()
                    + ":" + socket.getPort() + "\n");
            DataInputStream netIn = new DataInputStream(socket.getInputStream());
            netOut = new DataOutputStream(socket.getOutputStream());

            //receive public key
            byte[] foreignPublicKeyE;
            foreignPublicKeyE = new byte[256];
            netIn.read(foreignPublicKeyE);
            //split to decrypt. it was sent split so it would encrypt
            byte[] pk1E = new byte[foreignPublicKeyE.length / 2];
            byte[] pk2E = new byte[foreignPublicKeyE.length / 2];
            for (int i = 0; i < pk1E.length; i++) {
                pk1E[i] = foreignPublicKeyE[i];
                pk2E[i] = foreignPublicKeyE[pk1E.length + i];
            }
            byte[] pk1 = rsaCipher.decrypt(pk1E, rsaCipher.getKeyPair().getPrivate());
            byte[] pk2 = rsaCipher.decrypt(pk2E, rsaCipher.getKeyPair().getPrivate());
            byte[] foreignPublicKey = Shared.addArrays(pk1, pk2);

            clientPublicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(foreignPublicKey));

            //receive foreignID and N1received
            byte[] msgInE;
            int msgSize;
            msgInE = new byte[1024 * 1024];
            msgSize = netIn.read(msgInE);
            msgInE = Shared.shrinkArr(msgInE, msgSize);
            byte[] msgIn = rsaCipher.decrypt(msgInE, rsaCipher.getKeyPair().getPrivate());

            //process data
            byte[] N1received = new byte[BufferUtil.SIZEOF_INT];
            for (int i = 0; i < BufferUtil.SIZEOF_INT; i++) {
                N1received[i] = msgIn[i];
            }
            byte[] foreignIDArr = new byte[msgIn.length - BufferUtil.SIZEOF_INT];
            for (int i = BufferUtil.SIZEOF_INT; i < msgIn.length; i++) {
                foreignIDArr[i - BufferUtil.SIZEOF_INT] = msgIn[i];
            }
            foreignID = Shared.byteArrToString(foreignIDArr);

            //send N1received and N2
            byte[] msgOut;
            byte[] msgOutE;
            Random r = new Random(new Date().getTime());
            byte[] N2 = ByteBuffer.allocate(BufferUtil.SIZEOF_INT).putInt(r.nextInt(10000)).array();
            msgOut = Shared.addArrays(N1received, N2);
            msgOutE = rsaCipher.encrypt(msgOut, clientPublicKey);
            netOut.write(msgOutE);

            //receive N2
            msgInE = new byte[1024 * 1024];
            msgSize = netIn.read(msgInE);
            msgInE = Shared.shrinkArr(msgInE, msgSize);
            msgIn = rsaCipher.decrypt(msgInE, rsaCipher.getKeyPair().getPrivate());
            if (!Arrays.equals(msgIn, N2)) {
                System.out.println("N2 doesn't match!");
                socket.close();
                return;
            }

            //receive Ks
            msgInE = new byte[256];
            msgSize = netIn.read(msgInE);

            //split to decrypt. it was sent split so it would encrypt
            byte[] m1 = new byte[msgInE.length / 2];
            byte[] m2 = new byte[msgInE.length / 2];
            for (int i = 0; i < m1.length; i++) {
                m1[i] = msgInE[i];
                m2[i] = msgInE[m1.length + i];
            }
            byte[] m1E = rsaCipher.decrypt(m1, rsaCipher.getKeyPair().getPrivate());
            byte[] m2E = rsaCipher.decrypt(m2, rsaCipher.getKeyPair().getPrivate());
            msgIn = Shared.addArrays(m1E, m2E);
            byte[] keyArr = rsaCipher.decrypt(msgIn, clientPublicKey);
            desCipher.setKey(new SecretKeySpec(keyArr, 0, keyArr.length, "DES"));
            System.out.println("Secure connection established with client at: "
                    + socket.getInetAddress() + ":" + socket.getPort() + "\n");

            while (true) {
                //receive user's SIN and password
                msgInE = new byte[1024 * 1024];
                msgSize = netIn.read(msgInE);
                msgInE = Shared.shrinkArr(msgInE, msgSize);
                msgIn = desCipher.decrypt(msgInE, desCipher.getKey());
                String SIN = new String(msgIn);

                msgInE = new byte[1024 * 1024];
                msgSize = netIn.read(msgInE);
                msgInE = Shared.shrinkArr(msgInE, msgSize);
                msgIn = desCipher.decrypt(msgInE, desCipher.getKey());
                String password = new String(msgIn);

                //check if credentials are valid
                ArrayList<User> users = Voting.getUsers();
                boolean invalidUser = true;
                for (int i = 0; i < users.size(); i++) {
                    if (users.get(i).getSIN().equals(SIN)) {
                        if (users.get(i).getPassword().equals(password)) {
                            invalidUser = false;
                            break;
                        } else {
                            invalidUser = true;
                            break;
                        }
                    }
                }

                //check if user already voted
                if (!invalidUser) {
                    ArrayList<String> voted = Voting.getVotedSINs();
                    for (int i = 0; i < voted.size(); i++) {
                        if (voted.get(i).equals(SIN)) {
                            invalidUser = true;
                            break;
                        }
                    }
                }

                if (invalidUser) {
                    msgOut = "invalid".getBytes();
                    msgOutE = desCipher.encrypt(msgOut, desCipher.getKey());
                    netOut.write(msgOutE);
                    continue;
                }

                //generate UUID and check for duplicates
                boolean uuidInUse;
                String uuid;
                do{
                    uuidInUse = false;
                    uuid = UUID.randomUUID().toString();
                    ArrayList<String> uuids = Voting.getVotedUUIDs();
                    for (int i = 0; i < uuids.size(); i++) {
                        if(uuids.get(i).equals(uuid)){
                            uuidInUse = true;
                            break;
                        }
                    }
                } while(uuidInUse);
                
                //send UUID to CTF
                sendUUIDToCTF(uuid);

                //send UUID to client
                msgOut = uuid.getBytes();
                msgOutE = desCipher.encrypt(msgOut, desCipher.getKey());
                netOut.write(msgOutE);

                //mark user as alreadyVoted
                Voting.markVoted(SIN, uuid);
            }

        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(CLA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("CLA:\n");
        String ipCTF;
        int portCTF = 1000;
        if (args.length == 0) {
            ipCTF = "127.0.0.1";
        } else {
            ipCTF = args[0];
        }
        socketCTF = new Socket(ipCTF, portCTF);
        System.out.println("Connection established with CTF at: "
                + socketCTF.getInetAddress() + ":" + socketCTF.getPort() + "\n");

        //load the CTF public key
        CTFkey = KeyFile.LoadPubKey("CTF_public.key", "RSA");

        int port = 1001;
        ServerSocket ss = new ServerSocket(port);

        //load key pair from file
        keyPair = KeyFile.LoadKeyPair("CLA", "RSA");

        while (true) {
            //connect
            Socket socket = ss.accept();
            new Thread() {
                @Override
                public void run() {
                    serverInstance(socket);
                }
            }.start();
        }

    }

}
