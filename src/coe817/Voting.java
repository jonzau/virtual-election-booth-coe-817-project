package coe817;

import java.util.ArrayList;
import java.io.*;

/**
 *
 * @author Ionathan Zauritz, Jungro Yun
 */
public class Voting {

    public static ArrayList<Candidate> getCandidates() {
        ArrayList<Candidate> candidates = new ArrayList<>();
        //read from candidates.csv and populate candidates
        String csvFile = "candidates.csv";
        String line = "";
        BufferedReader br = null;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] commas = line.split(",");
                candidates.add(new Candidate(Integer.parseInt(commas[0]), commas[1]));
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return candidates;
    }

    public static ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();
        //read from candidates.csv and populate candidates
        String csvFile = "users.csv";
        String line = "";
        BufferedReader br = null;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] commas = line.split(",");
                users.add(new User(commas[0], commas[1]));
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    public static void markVoted(String SIN, String UUID) {
        String voteStr = SIN + "," + UUID + "\n";
        try {
            FileWriter writer = new FileWriter("voted.csv", true);
            writer.append(voteStr);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static ArrayList<String> getVotedSINs() {
        ArrayList<String> sins = new ArrayList<>();
        String csvFile = "voted.csv";
        String line = "";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] commas = line.split(",");
                sins.add(new String(commas[0]));
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sins;
    }
    
    public static ArrayList<String> getVotedUUIDs() {
        ArrayList<String> uuids = new ArrayList<>();
        String csvFile = "voted.csv";
        String line = "";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] commas = line.split(",");
                uuids.add(new String(commas[1]));
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uuids;
    }
    
    public static ArrayList<Integer> getCandidateIDsVotedFor() {
        ArrayList<Integer> ids = new ArrayList<>();
        String csvFile = "votes.csv";
        String line = "";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] commas = line.split(",");
                int tmp = Integer.parseInt(commas[1]);
                ids.add(tmp);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ids;
    }
    
    public static void saveVote(Vote vote) {
        String voteStr = vote.getUUID() + "," + vote.getCandidateId() + "\n";
        try {
            FileWriter writer = new FileWriter("votes.csv", true);
            writer.append(voteStr);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveUUID(String UUID) {
        try {
            FileWriter writer = new FileWriter("uuids.csv", true);
            writer.append(UUID + "\n");
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static ArrayList<String> getUUIDs() {
        ArrayList<String> uuids = new ArrayList<>();
        String csvFile = "uuids.csv";
        String line = "";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                uuids.add(new String(line));
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uuids;
    }
    
    public static boolean alreadyVoted(String UUID) {
        String csvFile = "votes.csv";
        String line = "";
        BufferedReader br = null;
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] commas = line.split(",");
                if (UUID.equals(commas[0])) {
                    return true;
                }
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
