package coe817;

import com.sun.prism.impl.BufferUtil;
import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Ionathan Zauritz, Jungro Yun
 */
public class CTF {

    private static final String ID = "CTF";
    private static KeyPair keyPair;
    private static PublicKey CLAkey;

    public static void serverInstance(Socket socket) {
        String foreignID = "";
        DataOutputStream netOut = null;
        PublicKey clientPublicKey;
        try {
            //create ciphers
            RSA rsaCipher = new RSA();
            rsaCipher.setKeyPair(keyPair);
            DES desCipher = new DES();
            System.out.println("Connecting client at: "
                    + socket.getInetAddress() + ":" + socket.getPort() + "\n");
            DataInputStream netIn = new DataInputStream(socket.getInputStream());
            netOut = new DataOutputStream(socket.getOutputStream());

            //receive public key
            byte[] foreignPublicKeyE;
            foreignPublicKeyE = new byte[256];
            netIn.read(foreignPublicKeyE);
            //split to decrypt. it was sent split so it would encrypt
            byte[] pk1E = new byte[foreignPublicKeyE.length / 2];
            byte[] pk2E = new byte[foreignPublicKeyE.length / 2];
            for (int i = 0; i < pk1E.length; i++) {
                pk1E[i] = foreignPublicKeyE[i];
                pk2E[i] = foreignPublicKeyE[pk1E.length + i];
            }
            byte[] pk1 = rsaCipher.decrypt(pk1E, rsaCipher.getKeyPair().getPrivate());
            byte[] pk2 = rsaCipher.decrypt(pk2E, rsaCipher.getKeyPair().getPrivate());
            byte[] foreignPublicKey = Shared.addArrays(pk1, pk2);

            clientPublicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(foreignPublicKey));

            //receive foreignID and N1received
            byte[] msgInE;
            int msgSize;
            msgInE = new byte[1024 * 1024];
            msgSize = netIn.read(msgInE);
            msgInE = Shared.shrinkArr(msgInE, msgSize);
            byte[] msgIn = rsaCipher.decrypt(msgInE, rsaCipher.getKeyPair().getPrivate());

            //process data
            byte[] N1received = new byte[BufferUtil.SIZEOF_INT];
            for (int i = 0; i < BufferUtil.SIZEOF_INT; i++) {
                N1received[i] = msgIn[i];
            }
            byte[] foreignIDArr = new byte[msgIn.length - BufferUtil.SIZEOF_INT];
            for (int i = BufferUtil.SIZEOF_INT; i < msgIn.length; i++) {
                foreignIDArr[i - BufferUtil.SIZEOF_INT] = msgIn[i];
            }
            foreignID = Shared.byteArrToString(foreignIDArr);

            //send N1received and N2
            byte[] msgOut;
            byte[] msgOutE;
            Random r = new Random(new Date().getTime());
            byte[] N2 = ByteBuffer.allocate(BufferUtil.SIZEOF_INT).putInt(r.nextInt(10000)).array();
            msgOut = Shared.addArrays(N1received, N2);
            msgOutE = rsaCipher.encrypt(msgOut, clientPublicKey);
            netOut.write(msgOutE);

            //receive N2
            msgInE = new byte[1024 * 1024];
            msgSize = netIn.read(msgInE);
            msgInE = Shared.shrinkArr(msgInE, msgSize);
            msgIn = rsaCipher.decrypt(msgInE, rsaCipher.getKeyPair().getPrivate());
            if (!Arrays.equals(msgIn, N2)) {
                System.out.println("N2 doesn't match!");
                socket.close();
                return;
            }

            //receive Ks
            msgInE = new byte[256];
            msgSize = netIn.read(msgInE);

            //split to decrypt. it was sent split so it would encrypt
            byte[] m1 = new byte[msgInE.length / 2];
            byte[] m2 = new byte[msgInE.length / 2];
            for (int i = 0; i < m1.length; i++) {
                m1[i] = msgInE[i];
                m2[i] = msgInE[m1.length + i];
            }
            byte[] m1E = rsaCipher.decrypt(m1, rsaCipher.getKeyPair().getPrivate());
            byte[] m2E = rsaCipher.decrypt(m2, rsaCipher.getKeyPair().getPrivate());
            msgIn = Shared.addArrays(m1E, m2E);
            byte[] keyArr = rsaCipher.decrypt(msgIn, clientPublicKey);
            desCipher.setKey(new SecretKeySpec(keyArr, 0, keyArr.length, "DES"));
            System.out.println("Secure connection established with client at: "
                    + socket.getInetAddress() + ":" + socket.getPort() + "\n");

            while (true) {
                //recieve request to vote from client
                msgInE = new byte[1024 * 1024];
                msgSize = netIn.read(msgInE);
                msgInE = Shared.shrinkArr(msgInE, msgSize);
                msgIn = desCipher.decrypt(msgInE, desCipher.getKey());

                if (!new String(msgIn).equals("request")) {
                    continue;
                }
                //send list of candidates
                String candidatesStr = "";
                ArrayList<Candidate> candidates = Voting.getCandidates();
                for (int i = 0; i < candidates.size(); i++) {
                    candidatesStr += "[" + candidates.get(i).getId() + "] " + candidates.get(i).getName() + "\n";
                }
                msgOut = candidatesStr.getBytes();
                msgOutE = desCipher.encrypt(msgOut, desCipher.getKey());
                netOut.write(msgOutE);

                //receive vote (candidate number and voting number)
                boolean invalid = false;
                Vote vote = new Vote(-1, "");

                msgInE = new byte[1024 * 1024];
                msgSize = netIn.read(msgInE);
                msgInE = Shared.shrinkArr(msgInE, msgSize);
                msgIn = desCipher.decrypt(msgInE, desCipher.getKey());
                vote.setUUID(new String(msgIn));

                msgInE = new byte[1024 * 1024];
                msgSize = netIn.read(msgInE);
                msgInE = Shared.shrinkArr(msgInE, msgSize);
                msgIn = desCipher.decrypt(msgInE, desCipher.getKey());
                vote.setCandidateId(Integer.parseInt(new String(msgIn)));

                //check if already voted
                if (Voting.alreadyVoted(vote.getUUID())) {
                    invalid = true;
                    System.out.println("CTF: already voted");
                }

                //check if candidate exists
                if (vote.getCandidateId() > Voting.getCandidates().size() || vote.getCandidateId() <= 0) {
                    invalid = true;
                }

                //check if valid UUID
                ArrayList<String> uuids = Voting.getUUIDs();
                if (!invalid) {
                    invalid = true;
                    for (int i = 0; i < uuids.size(); i++) {
                        if (uuids.get(i).equals(vote.getUUID())) {
                            invalid = false;
                            break;
                        }
                    }
                }

                if (invalid) {
                    String invalidstr = "invalid";
                    msgOut = invalidstr.getBytes();
                    msgOutE = desCipher.encrypt(msgOut, desCipher.getKey());
                    netOut.write(msgOutE);
                    continue;
                }

                //save vote to file
                Voting.saveVote(vote);
                
                //send confirmation and result so far
                ArrayList<Integer> ids = Voting.getCandidateIDsVotedFor();
                candidatesStr = "Voted for candidate: " + vote.getCandidateId() 
                        + "\nPreliminary Results:\n";
                for (int i = 0; i < candidates.size(); i++) {
                    int count = 0;
                    for (int j = 0; j < ids.size(); j++) {
                        if (ids.get(j) == candidates.get(i).getId()) {
                            count++;
                        }
                    }
                    candidatesStr += "[" + candidates.get(i).getId() + "] "
                            + candidates.get(i).getName() + " - Votes: " + count + "\n";
                }

                msgOut = candidatesStr.getBytes();
                msgOutE = desCipher.encrypt(msgOut, desCipher.getKey());
                netOut.write(msgOutE);
            }

        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(CTF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("CTF:\n");
        int port = 1000;
        ServerSocket ss = new ServerSocket(port);
        //load the key. it should go into a static variable at the top
        CLAkey = KeyFile.LoadPubKey("CLA_public.key", "RSA");

        Socket socketCLA = ss.accept();
        System.out.println("Connection established with CLA at: "
                + socketCLA.getInetAddress() + ":" + socketCLA.getPort() + "\n");
        new Thread() {
            @Override
            public void run() {
                try {
                    DataInputStream netIn = new DataInputStream(socketCLA.getInputStream());
                    RSA rsaCipher = new RSA();
                    byte[] msgInE;
                    int msgSize;
                    byte[] msgIn;
                    while (true) {
                        //use socketServer to receive voting number
                        msgInE = new byte[1024 * 1024];
                        msgSize = netIn.read(msgInE);
                        msgInE = Shared.shrinkArr(msgInE, msgSize);
                        msgIn = rsaCipher.decrypt(msgInE, keyPair.getPrivate());
                        String UUID = new String(msgIn);

                        //save voting number to a file
                        Voting.saveUUID(UUID);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

        port = 1002;
        ss = new ServerSocket(port);

        //load key pair from file
        keyPair = KeyFile.LoadKeyPair("CTF", "RSA");

        while (true) {
            //connect
            Socket socket = ss.accept();
            new Thread() {
                @Override
                public void run() {
                    serverInstance(socket);
                }
            }.start();
        }

    }

}
