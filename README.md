# README #

This is a project for COE817 (Network Security) - Winter/2016.
The project consists of a Virtual Election Booth that uses symmetric and asymmetric encryption to implement a secure election protocol. The design consists of two central facilities: Central Tabulating Facility (CTF) and Central Legitimization Agency (CLA), as well as a client which connects to the facilities in order to vote.
The project was implemented in Java using the Java cryptography class libraries, and Java socket communication programming.

Members:
Jungro Yun - jung.yun@ryerson.ca,
Ionathan Zauritz - izauritz@ryerson.ca